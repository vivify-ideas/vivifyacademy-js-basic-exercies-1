/* 11. Data je sledeća funkcija, koja prolazi kroz niz pomocu for petlje i ispisuje poslednji element:

    function lastValueWithFor() {
        var array = [10, 20, 30, 40, 50];

        for (var i = 0; i < array.length; i++) {
            var currentElement = array[i];
        }

        var lastElement = currentElement;
        console.log(lastElement);
    }

Ispod je data verzija te funkcija koja koristi forEach umesto for:

    function lastValueWithForEach() {
        var array = [10, 20, 30, 40, 50];

        array.forEach(function (element) {
            var currentElement = element;
        });

        var lastElement = currentElement;
        console.log(lastElement);
    }

Druga funkcija ne postiže isti rezultat kao prva, već se javlja greška. Pronaći uzrok greške i promeniti funkciju tako da radi isto što i prva.
*/

function lastValueWithFor() {
    var array = [10, 20, 30, 40, 50];

    for (var i = 0; i < array.length; i++) {
        var currentElement = array[i];
    }

    var lastElement = currentElement;
    console.log(lastElement);
}

function fixedLastValueWithForEach() {
    var array = [10, 20, 30, 40, 50];
    var currentElement;

    array.forEach(function (element) {
        // Ako se ovde (unutar funkcije) definise promenljiva sa var,
        // ona je lokalna i ne moze joj se pristupiti od spolja
        currentElement = element;
    });

    var lastElement = currentElement;
    console.log(lastElement);
}
