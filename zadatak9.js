// 9. Napisati funkciju koja prima dva niza, array1 i array2, i poziva forEach na prvom pa na drugom nizu, pritom koristeći kao callback istu funkciju (ne treba pisati dva puta isti kod za callback).

function logElementsOfBoth(array1, array2) {
	var logFunc = function(element) {
        console.log(element);
    };

	array1.forEach(logFunc);
	array2.forEach(logFunc);
}
