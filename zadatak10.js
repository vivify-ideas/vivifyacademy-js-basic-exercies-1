// 10. Napisati funkciju koja prima proizvoljni niz i neki broj N, i ispisuje elemente niza, uvećane za N, pomoću forEach.

function logElementsPlusN(array, n) {
    array.forEach(function(element) {
        console.log(element + n);
    });
}
