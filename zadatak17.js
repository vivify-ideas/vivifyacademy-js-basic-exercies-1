/* 17. Napraviti objekat polyglot, koji sadrzi sledeće property-e:

    - name, predstavlja ime osobe
    - languagesSpoken, predstavlja niz jezika koje ta osoba govori

Dodati na objekat polyglot metod logLanguages, koji pomoću forEach prolazi kroz niz languagesSpoken, i ispisuje za svaki jezik “<name> govori <language>”. Npr. ako je name Pera a languagesSpoken je ['engleski', 'francuski', 'nemacki'], program treba da ispiše:

    Pera govori engleski
    Pera govori francuski
    Pera govori nemacki

Ukoliko program ne radi kako je očekivano, probati isto sa for petljom umesto forEach, i razmotriti razlike izmedju ta dva slučaja. Konačno rešenje treba da koristi forEach.
*/

var polyglot = {
    name: 'Pera',
    languages: ['engleski', 'francuski', 'nemacki']
};

polyglot.logLanguages = function() {
    var self = this;
    this.languages.forEach(function(language) {
        // Ako bi se ovde koristilo this.name umesto self.name, program ne bi radio.
        // To je zbog toga sto se ovde nalazimo u novoj funkciji, a ne direktno u metodi logLanguages.
        // Posto ova (unutrasnja, anonimna) funkcija nije pozvana kao metod na objektu polyglog, this nije isto sto i polyglot.
        // Ovo se zaobilazi tako sto se this sacuva u promenljivoj self koju definisemo direktno u logLanguages.
        
        console.log(self.name + ' govori ' + language);
    });
}
