// 4. Napisati funkciju fahrenheitToCelsius, koja prima temperaturu u stepenima Farenhajta (°F) i vraća odgovarajuću temperaturu u stepenima Celzijusa (°C). Formula za konverziju je [°C] = ([°F] − 32) × ​5⁄9

function fahrenheitToCelsius(degreesFahrenheit) {
    return (degreesFahrenheit - 32) * 5 / 9;
}
