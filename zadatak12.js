/* 12. Slično kao u prethodnom zadatku, date su dve verzije iste funkcije, sa for i sa forEach:

    function sumWithFor() {
        var array = [10, 20, 30, 40, 50];
        var sum = 0;

        for (var i = 0; i < array.length; i++) {
            var sum = sum + array[i];
        }

        console.log(sum);
    }

    function sumWithForEach() {
        var array = [10, 20, 30, 40, 50];
        var sum = 0;

        array.forEach(function (element) {
            var sum = sum + element;
        });

        console.log(sum);
    }

    Utvrditi zašto druga funkcija ne proizvodi isti rezultat kao prva, i ispraviti grešku.
*/

function sumWithFor() {
    var array = [10, 20, 30, 40, 50];
    var sum = 0;

    for (var i = 0; i < array.length; i++) {
        var sum = sum + array[i];
    }

    console.log(sum);
}

function fixedSumWithForEach() {
    var array = [10, 20, 30, 40, 50];
    var sum = 0;

    array.forEach(function (element) {
        // Uklonjen je var iz ove funkcije
        // Cak i ako postoji promenljiva sa istim tim imenom spolja,
        // ako se sa var definise promenljiva unutar funkcije, stvara se nova.
        // Zbog ovoga nije bilo moguce uticati na vrednost promenljive sum definisane iznad.
        sum = sum + element;
    });

    console.log(sum);
}
