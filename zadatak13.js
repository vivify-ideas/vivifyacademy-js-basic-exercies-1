// 13. Napisati funkciju koja prima kao argumente niz i callback funkciju, prolazi kroz niz pomoću for petlje i u svakom koraku poziva prosleđenu funkciju sa trenutnim elementom niza kao argumentom. (Drugim rečima, funkcija treba da radi isto što i forEach.)

function ourForEach(array, callback) {
    for (var i = 0; i < array.length; i++) {
        callback(array[i]);
    }
}
