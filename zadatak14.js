// 14. Napraviti objekat person1 sa property-ima name, age, gender. Napraviti drugi objekat, person1Copy, i prekopirati sve property-e sa person1 na person1Copy.

var person1 = {
    name: 'Laza',
    age: 31,
    gender: 'M'
};

var person1Copy = {};
person1Copy.name = person1.name;
person1Copy.age = person1.age;
person1Copy.gender = person1.gender;
