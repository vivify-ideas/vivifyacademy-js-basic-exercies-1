// 8. Napisati funkciju koja prima proizvoljni niz i ispisuje njegove elemente, uvećane za 1, pomoću forEach.

function logElementsPlus1(array) {
    array.forEach(function(element) {
        console.log(element + 1);
    });
}
