// 15. Napraviti objekat user1 koji ima property-e username i password, i metod changePassword, koji prima novu šifru i na trenutnom objektu (this) postavlja novu vrednost property-a password. Postaviti početni username i password, zatim promeniti šifru pomoću changePassword.

var user1 = {
    username: 'user1',
    password: 'old password',
    changePassword: function(newPassword) {
        this.password = newPassword;
    }
};

user1.changePassword('new password');
