// 3. Napisati funkciju celsiusToFahrenheit koja prima temperaturu u stepenima Celzijusa (°C) i vraća odgovarajuću temperaturu u stepenima Farenhajta (°F). Formula za konverziju je sledeća: [°F] = [°C] × ​9⁄5 + 32.

function celsiusToFahrenheit(degreesCelsius) {
    return degreesCelsius * 9 / 5 + 32;
}
