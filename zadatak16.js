/* 16. Napraviti objekat user2 koji ima property-e username i password. Izvrsiti sledeći kod:

    user2.changePassword = user1.changePassword;
    user2.changePassword('new password');

Utvrditi da li je promenjena šifra na objektu user1 ili user2, i zašto. Dodati na user1 metod changeUser1Password, koji će uvek promeniti sifru na objektu user1, čak i ako se prekopira na drugi objekat:

    user2.changeUser1Password = user1.changeUser1Password;
    user2.changeUser1Password('another new password');
*/

var user1 = {
    username: 'user1',
    password: 'old password',
    changePassword: function(newPassword) {
        this.password = newPassword;
    }
};

var user2 = {
    username: 'user2',
    password: 'old password'
};

user2.changePassword = user1.changePassword;
user2.changePassword('new password');

console.log(user1.password);    // old password
console.log(user2.password);    // new password

user1.changeUser1Password = function(newPassword) {
    user1.password = newPassword;
}

user2.changeUser1Password = user1.changeUser1Password;
user2.changeUser1Password('another new password');

console.log(user1.password);    // another new password
console.log(user2.password);    // new password
